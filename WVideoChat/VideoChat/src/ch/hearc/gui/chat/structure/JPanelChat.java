﻿
package ch.hearc.gui.chat.structure;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import ch.hearc.gui.tools.Appearence;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelChat extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelChat()
		{
		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		panelChatCenter = new JPanelChatCenter();
		panelChatHeader = new JPanelChatHeader();
		panelChatFooter = new JPanelChatFooter();

		// Layout : Specification
			{
			BorderLayout borderLayout = new BorderLayout();
			setLayout(borderLayout);
			}

		// JComponent : add
		add(panelChatHeader, BorderLayout.NORTH);
		add(panelChatCenter, BorderLayout.CENTER);
		add(panelChatFooter, BorderLayout.SOUTH);
		}

	private void control()
		{
		// nothing
		}

	private void appearance()
		{
		this.setBackground(Appearence.CYAN50);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JPanelChatCenter panelChatCenter;
	private JPanelChatHeader panelChatHeader;
	private JPanelChatFooter panelChatFooter;
	}
