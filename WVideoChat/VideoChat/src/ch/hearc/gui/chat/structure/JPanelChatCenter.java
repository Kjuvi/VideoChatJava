﻿
package ch.hearc.gui.chat.structure;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import ch.hearc.gui.chat.text.JPanelChatMessage;
import ch.hearc.gui.chat.video.JPanelWebcams;
import ch.hearc.gui.tools.Appearence;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelChatCenter extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelChatCenter()
		{
		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		panelWebcams = new JPanelWebcams();
		panelChatMessage = new JPanelChatMessage();

		// Layout : Specification
			{
			BorderLayout borderlayout = new BorderLayout();
			setLayout(borderlayout);

			// flowlayout.setHgap(20);
			// flowlayout.setVgap(20);
			}

		// JComponent : add
		add(panelWebcams, BorderLayout.CENTER);
		add(panelChatMessage, BorderLayout.SOUTH);
		}

	private void control()
		{
		// nothing
		}

	private void appearance()
		{
		this.setBackground(Appearence.CYAN50);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JPanelWebcams panelWebcams;
	private JPanelChatMessage panelChatMessage;
	}
