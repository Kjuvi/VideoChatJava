﻿
package ch.hearc.gui.chat.structure;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import ch.hearc.gui.tools.Appearence;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelChatHeader extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelChatHeader()
		{
		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation

		// Layout : Specification
			{
			FlowLayout flowlayout = new FlowLayout(FlowLayout.CENTER);
			setLayout(flowlayout);

			flowlayout.setHgap(20);
			flowlayout.setVgap(20);
			}

		// JComponent : add
		}

	private void control()
		{
		// Nothing
		}

	private void appearance()
		{
		setBackground(Appearence.CYAN700);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	}
