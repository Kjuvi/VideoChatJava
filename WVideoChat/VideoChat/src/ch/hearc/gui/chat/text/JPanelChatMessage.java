﻿
package ch.hearc.gui.chat.text;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JPanel;

import ch.hearc.gui.tools.Appearence;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelChatMessage extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelChatMessage()
		{
		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		panelReceive = new JPanelReceive();
		panelSend = new JPanelSend();

		// Layout : Specification
			{
			BorderLayout borderlayout = new BorderLayout();
			setLayout(borderlayout);
			}

		Box vbox = createVerticalBox();

		// JComponent : add
		add(vbox);
		}

	private Box createVerticalBox()
		{
		Box vbox = Box.createVerticalBox();

		vbox.add(Box.createVerticalStrut(10));
		vbox.add(panelReceive);
		vbox.add(Box.createVerticalStrut(10));
		vbox.add(panelSend);
		vbox.add(Box.createVerticalStrut(10));

		return vbox;
		}

	private void control()
		{
		// nothing
		}

	private void appearance()
		{
		setBackground(Appearence.CYAN50);

		Dimension dimension = new Dimension(750, 300);
		setMinimumSize(dimension);
		setPreferredSize(dimension);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JPanelReceive panelReceive;
	private JPanelSend panelSend;
	}
