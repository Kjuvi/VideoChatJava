﻿
package ch.hearc.gui.chat.video;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;

import ch.hearc.gui.chat.video.videopanel.JPanelLocalVideo;
import ch.hearc.gui.chat.video.videopanel.JPanelRemoteVideo;
import ch.hearc.gui.tools.Appearence;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelWebcams extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelWebcams()
		{
		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		jPanelLocalVideo = new JPanelLocalVideo();
		jPanelRemoteVideo = new JPanelRemoteVideo(jPanelLocalVideo);
		jPanelVideoAction = new JPanelVideoAction(jPanelLocalVideo, jPanelRemoteVideo);

		// Layout : Specification
			{
			BorderLayout borderlayout = new BorderLayout();
			setLayout(borderlayout);
			}

		// JComponent : add
		add(jPanelRemoteVideo, BorderLayout.CENTER);
		add(jPanelVideoAction, BorderLayout.EAST);
		}

	private void control()
		{
		// nothing
		}

	private void appearance()
		{
		setBackground(Appearence.CYAN700);

		Dimension dimension = new Dimension(getWidth(), 250);
		setMinimumSize(dimension);
		setPreferredSize(dimension);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JPanelLocalVideo jPanelLocalVideo;
	private JPanelRemoteVideo jPanelRemoteVideo;
	private JPanelVideoAction jPanelVideoAction;
	}
