﻿
package ch.hearc.gui.chat.video.videopanel;

import java.awt.Image;

import javax.swing.JPanel;

import ch.hearc.video.image.WebcamImage;

/**
 * @author Axel Rieben, André Da Silva, Quentin Vaucher
 */
public class JPanelVideo extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelVideo()
		{
		panelWidth = 1; //  Avoid resizing an image to 0 at startup,
		panelHeight = 1; // which leads to an error!
		horizontalGap = 0;
		}

	public void updateHorizontalGap()
		{
		// Wait for the first image
		while(horizontalGap == 0)
			{
			try
				{
				updateDimensions();
				Thread.sleep(10);
				}
			catch (InterruptedException e)
				{
				e.printStackTrace();
				}
			}
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Protected						*|
	\*------------------------------------------------------------------*/

	protected void updateDimensions()
		{
		if (webcamImage != null)
			{
			panelWidth = getWidth();
			panelHeight = getHeight();

			horizontalGap = (panelWidth - scaledImage.getWidth(null)) / 2;
			}
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	protected WebcamImage webcamImage;
	protected Image scaledImage;

	protected int horizontalGap;

	protected int panelWidth;
	protected int panelHeight;
	}
