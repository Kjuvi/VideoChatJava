
package ch.hearc.gui.connection.structure;

import java.awt.FlowLayout;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.JLabel;
import javax.swing.JPanel;

import ch.hearc.gui.tools.Appearence;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class JPanelConnectionFooter extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelConnectionFooter()
		{
		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		String localhostIp = "";

		try
			{
			localhostIp = InetAddress.getLocalHost().getHostAddress();
			}
		catch (UnknownHostException e)
			{
			e.printStackTrace();
			}

		labelYourIP = new JLabel("Your IP : " + localhostIp);

		// Layout : Specification
			{
			FlowLayout flowlayout = new FlowLayout(FlowLayout.LEFT);
			setLayout(flowlayout);

			flowlayout.setHgap(20);
			flowlayout.setVgap(20);
			}

		// JComponent : add
		add(labelYourIP);
		}

	private void control()
		{
		// nothing
		}

	private void appearance()
		{
		labelYourIP.setForeground(Appearence.CYAN50);
		setBackground(Appearence.CYAN700);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JLabel labelYourIP;

	}
