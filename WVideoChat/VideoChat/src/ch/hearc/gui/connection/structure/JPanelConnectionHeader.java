
package ch.hearc.gui.connection.structure;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JPanel;

import ch.hearc.gui.tools.Appearence;
import ch.hearc.gui.tools.ImageStore;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class JPanelConnectionHeader extends JPanel
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public JPanelConnectionHeader()
		{
		geometry();
		control();
		appearance();
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void geometry()
		{
		// JComponent : Instanciation
		labelTitle = new JLabel(TITLE);
		labelImage = new JLabel();

		// Layout : Specification
			{
			FlowLayout flowlayout = new FlowLayout(FlowLayout.CENTER);
			setLayout(flowlayout);

			flowlayout.setHgap(40);
			flowlayout.setVgap(20);
			}

		// JComponent : add
		add(labelImage);
		add(labelTitle);
		}

	private void control()
		{
		// nothing
		}

	private void appearance()
		{
		labelImage.setIcon(ImageStore.CHAT);

		try
			{
			Font font = Font.createFont(Font.TRUETYPE_FONT, new File(this.getClass().getClassLoader().getResource("font/Roboto-Bold.ttf").getFile()));
			labelTitle.setFont(font.deriveFont(32.0f));
			}
		catch (FontFormatException | IOException e)
			{
			e.printStackTrace();
			}

		labelTitle.setForeground(Appearence.CYAN50);
		setBackground(Appearence.CYAN700);
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Tools
	private JLabel labelImage;
	private JLabel labelTitle;

	/*------------------------------*\
	|*			  Static			*|
	\*------------------------------*/

	private static final String TITLE = "Java Chat 2.0";
	}
