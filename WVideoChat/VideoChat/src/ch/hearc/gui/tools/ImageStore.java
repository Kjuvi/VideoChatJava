
package ch.hearc.gui.tools;

import javax.swing.ImageIcon;

public class ImageStore
	{

	/*------------------------------------------------------------------*\
	|*		Version Assynchrone	(non bloquant)							*|
	\*------------------------------------------------------------------*/

	public static final ImageIcon CHAT = ImageLoader.loadAsynchrone("/ressources/image/chat_icon.png");
	public static final ImageIcon CONNECT = ImageLoader.loadAsynchrone("/ressources/image/connect.png");
	public static final ImageIcon CLOSE = ImageLoader.loadAsynchrone("/ressources/image/close.png");

	public static final ImageIcon WEBCAM = ImageLoader.loadAsynchrone("/ressources/image/webcam.png");
	public static final ImageIcon WEBCAM_ACTIVE = ImageLoader.loadAsynchrone("/ressources/image/webcam_active.png");
	public static final ImageIcon WEBCAM_KILL = ImageLoader.loadAsynchrone("/ressources/image/webcam_kill.png");
	public static final ImageIcon WEBCAM_KILL_ACTIVE = ImageLoader.loadAsynchrone("/ressources/image/webcam_kill_active.png");

	public static final ImageIcon MIRROR = ImageLoader.loadAsynchrone("/ressources/image/mirror.png");
	public static final ImageIcon MIRROR_ACTIVE = ImageLoader.loadAsynchrone("/ressources/image/mirror_active.png");

	public static final ImageIcon GRAYSCALE = ImageLoader.loadAsynchrone("/ressources/image/grayscale.png");
	public static final ImageIcon GRAYSCALE_ACTIVE = ImageLoader.loadAsynchrone("/ressources/image/grayscale_active.png");
	public static final ImageIcon GRAYSCALE_KILL = ImageLoader.loadAsynchrone("/ressources/image/grayscale_kill.png");
	public static final ImageIcon GRAYSCALE_KILL_ACTIVE = ImageLoader.loadAsynchrone("/ressources/image/grayscale_kill_active.png");

	public static final ImageIcon PORTRAIT = ImageLoader.loadAsynchrone("/ressources/image/portrait.png");
	public static final ImageIcon PORTRAIT_ACTIVE = ImageLoader.loadAsynchrone("/ressources/image/portrait_active.png");
	public static final ImageIcon PORTRAIT_KILL = ImageLoader.loadAsynchrone("/ressources/image/portrait_kill.png");
	public static final ImageIcon PORTRAIT_KILL_ACTIVE = ImageLoader.loadAsynchrone("/ressources/image/portrait_kill_active.png");

	public static final ImageIcon SEND = ImageLoader.loadAsynchrone("/ressources/image/send.png");
	public static final ImageIcon SEND_ACTIVE = ImageLoader.loadAsynchrone("/ressources/image/send_active.png");
	}
