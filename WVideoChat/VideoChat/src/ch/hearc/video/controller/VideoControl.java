
package ch.hearc.video.controller;

import ch.hearc.gui.chat.video.videopanel.JPanelLocalVideo;
import ch.hearc.rmi.Application;
import ch.hearc.video.image.WebcamImage;

/**
 * @author Axel Rieben, Andr� Da Silva, Quentin Vaucher
 */
public class VideoControl implements Runnable
	{

	/*------------------------------------------------------------------*\
	|*							Constructeurs							*|
	\*------------------------------------------------------------------*/

	public static synchronized VideoControl getInstance()
		{
		if (INSTANCE == null)
			{
			INSTANCE = new VideoControl();
			}
		return INSTANCE;
		}

	/**
	 * Set the panel for which the VideoControl will have access.
	 * @param panelVideo The panel in which the video is displayed
	 */
	public void initVideoControl(JPanelLocalVideo panelVideo, WebcamImage webcamImage)
		{
		this.panelLocalVideo = panelVideo;
		this.webcamImage = webcamImage;
		}

	private VideoControl()
		{
		isInterrupted = true; // No video by default
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Public							*|
	\*------------------------------------------------------------------*/

	public synchronized void startVideo()
		{
		isInterrupted = false;
		threadRemoteVideo = new Thread(this);
		threadRemoteVideo.start();
		}

	public void stopVideo()
		{
		isInterrupted = true;
		}

	@Override
	public void run()
		{
		while(!isInterrupted)
			{
			if (webcamImage.getWebcam().isImageNew())
				{
				// Update remote image
				webcamImage.updateImge();
				Application.getInstance().sendImage(webcamImage);

				// Update local image
				panelLocalVideo.repaint();

				sleepFPS(60); // Max 60fps, but can be less if image is not new !
				}
			}

		threadRemoteVideo.interrupt();
		}

	public boolean isRunning()
		{
		return !isInterrupted;
		}

	/*------------------------------------------------------------------*\
	|*							Methodes Private						*|
	\*------------------------------------------------------------------*/

	private void sleepFPS(int fps)
		{
		try
			{
			Thread.sleep(1000 / fps);
			}
		catch (InterruptedException e)
			{
			e.printStackTrace();
			System.err.println("Error during fps sleep.");
			}
		}

	/*------------------------------------------------------------------*\
	|*							Attributs Private						*|
	\*------------------------------------------------------------------*/

	// Inputs
	private JPanelLocalVideo panelLocalVideo;
	private WebcamImage webcamImage;

	// Tools
	private boolean isInterrupted;
	private Thread threadRemoteVideo;

	/*------------------------------*\
	|*			  Static			*|
	\*------------------------------*/

	private static VideoControl INSTANCE = null;
	}
